

# import palette_mandmap
# from sys import argv, exit

import os
import strformat
import strutils
import "./palette_mandmap.nim"

proc calc_mandelbrot(width: int, height: int, maxiter: int, palette: Pallete)=
    var zx: float64
    var zy: float64
    # var  zx2: float64
    # var  zy2: float64
    var  cx: float
    var  cy: float
    # var  r: int
    # var  g: int
    # var  b: int
    var  i: int

    echo "P3"
    echo &"{width} {height}"
    echo "255"
    cy = -1.5

    for y in 0..height:
        cx = -2.0
        for x in 0..width:
            zx = 0.0
            zy = 0.0
            i = 0
            while i < maxiter:
                let zx2 = zx * zx
                let zy2 = zy * zy
                if zx2 + zy2 > 4.0:
                    break
                zy = 2.0 * zx * zy + cy
                zx = zx2 - zy2 + cx
                i += 1

            let r = palette[i][0]
            let g = palette[i][1]
            let b = palette[i][2]
            # (r,g,b) = palette[i]
            echo &"{r} {g} {b}"
            cx += 3.0/width.toFloat()
        cy += 3.0/height.toFloat()


when isMainModule:
    if paramCount() < 3:
        echo "usage: mandelbrot_nim width height maxiter"
        quit(1)

    let width = parseInt(paramStr 1)
    let height = parseInt(paramStr 2)
    let maxiter = parseInt(paramStr 3)
    calc_mandelbrot(width, height, maxiter, palette_mandmap.palette)
    